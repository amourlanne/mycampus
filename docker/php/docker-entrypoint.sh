#!/bin/bash

useradd myeducation_user --shell /bin/bash --home /home/myeducation_user --uid="1000"
mkdir /home/myeducation_user
chown myeducation_user:myeducation_user /home/myeducation_user
# virtual volume folder
chown myeducation_user:myeducation_user /var/www/var

su -c "php /var/www/bin/console cache:clear -n" myeducation_user

# Ensure that the key pair for JWT are created
if [ ! -f /var/www/config/jwt/private.pem ]; then
  echo "Generate key pair fot jwt authentication"
  chown myeducation_user:myeducation_user /var/www/config/jwt
  # generate private key
  su -c "openssl genrsa -out /var/www/config/jwt/private.pem 4096" myeducation_user
  # generate public key
  su -c "openssl pkey -in /var/www/config/jwt/private.pem -out /var/www/config/jwt/public.pem -pubout" myeducation_user
fi

echo "Installing dependencies ..."
su -c "composer install " myeducation_user

nginx
php-fpm
