#!/bin/bash

userdel node

useradd myeducation_user --shell /bin/bash --home /home/myeducation_user --uid="1000"
mkdir /home/myeducation_user
chown myeducation_user:myeducation_user /home/myeducation_user

# referred to setup-env.sh
/setup-env.sh /var/www/public

su -c "yarn install" myeducation_user
su -c "yarn serve" myeducation_user
