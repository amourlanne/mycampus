stages:
  - build
  - security
  - quality
  - test
  - deploy

# Use same runner
default:
  tags:
    - myeducation-docker

variables:
  GIT_STRATEGY: none

# Stage BUILD
.buildjob:
  stage: build
  services:
    - docker:dind
  image: docker
  variables:
    GIT_STRATEGY: fetch

## Job BUILD SERVER
build:server:
  extends: .buildjob
  variables:
    SOURCE_PATH: src/server
  script:
    - export VERSION=$(git describe --tags --always)
    # Connect to the registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    # Retrieve master if exists
    - docker pull $CI_REGISTRY_IMAGE/php-server:master || true
    # Build and use cache from master build
    - >-
      docker build
      --cache-from $CI_REGISTRY_IMAGE/php-server:master
      --rm=false
      -t $CI_REGISTRY_IMAGE/php-server:${CI_COMMIT_REF_SLUG}
      --build-arg VERSION=${VERSION}
      --build-arg SOURCE_PATH=${SOURCE_PATH}
      --target prod
      --file docker/php/Dockerfile
      .
    # Make sure we're still connected to the registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    # Push the image
    - docker push $CI_REGISTRY_IMAGE/php-server:${CI_COMMIT_REF_SLUG}

## Job BUILD ORGANIZATION
build:organization:
  extends: .buildjob
  variables:
    SOURCE_PATH: src/organization
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker pull $CI_REGISTRY_IMAGE/dev/node-organization:master || true
    - >-
      docker build
      --cache-from $CI_REGISTRY_IMAGE/dev/node-organization:master
      --rm=false
      -t $CI_REGISTRY_IMAGE/dev/node-organization:${CI_COMMIT_REF_SLUG}
      --build-arg SOURCE_PATH=${SOURCE_PATH}
      --target build
      --file docker/node/Dockerfile
      .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker push $CI_REGISTRY_IMAGE/dev/node-organization:${CI_COMMIT_REF_SLUG}
    - >-
      docker build
      --cache-from $CI_REGISTRY_IMAGE/dev/node-organization:${CI_COMMIT_REF_SLUG}
      --rm=false
      -t $CI_REGISTRY_IMAGE/node-organization:${CI_COMMIT_REF_SLUG}
      --build-arg SOURCE_PATH=${SOURCE_PATH}
      --target prod
      --file docker/node/Dockerfile
      .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker push $CI_REGISTRY_IMAGE/node-organization:${CI_COMMIT_REF_SLUG}

## Job BUILD ADMIN
build:admin:
  extends: .buildjob
  variables:
    SOURCE_PATH: src/admin
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker pull $CI_REGISTRY_IMAGE/dev/node-admin:master || true
    - >-
      docker build
      --cache-from $CI_REGISTRY_IMAGE/dev/node-admin:master
      --rm=false
      -t $CI_REGISTRY_IMAGE/dev/node-admin:${CI_COMMIT_REF_SLUG}
      --build-arg SOURCE_PATH=${SOURCE_PATH}
      --target build
      --file docker/node/Dockerfile
      .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker push $CI_REGISTRY_IMAGE/dev/node-admin:${CI_COMMIT_REF_SLUG}
    - >-
      docker build
      --cache-from $CI_REGISTRY_IMAGE/dev/node-admin:${CI_COMMIT_REF_SLUG}
      --rm=false
      -t $CI_REGISTRY_IMAGE/node-admin:${CI_COMMIT_REF_SLUG}
      --build-arg SOURCE_PATH=${SOURCE_PATH}
      --target prod
      --file docker/node/Dockerfile
      .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker push $CI_REGISTRY_IMAGE/node-admin:${CI_COMMIT_REF_SLUG}

## Job BUILD CLIENT
build:client:
  extends: .buildjob
  variables:
    SOURCE_PATH: src/client
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker pull $CI_REGISTRY_IMAGE/dev/node-client:master || true
    - >-
      docker build
      --cache-from $CI_REGISTRY_IMAGE/dev/node-client:master
      --rm=false
      -t $CI_REGISTRY_IMAGE/dev/node-client:${CI_COMMIT_REF_SLUG}
      --build-arg SOURCE_PATH=${SOURCE_PATH}
      --target build
      --file docker/node/Dockerfile
      .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker push $CI_REGISTRY_IMAGE/dev/node-client:${CI_COMMIT_REF_SLUG}
    - >-
      docker build
      --cache-from $CI_REGISTRY_IMAGE/dev/node-client:${CI_COMMIT_REF_SLUG}
      --rm=false
      -t $CI_REGISTRY_IMAGE/node-client:${CI_COMMIT_REF_SLUG}
      --build-arg SOURCE_PATH=${SOURCE_PATH}
      --target prod
      --file docker/node/Dockerfile
      .
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker push $CI_REGISTRY_IMAGE/node-client:${CI_COMMIT_REF_SLUG}

.serverjob:
  image:
    name: $CI_REGISTRY_IMAGE/php-server:${CI_COMMIT_REF_SLUG}
    entrypoint: [""]

.organizationjob:
  image:
    name: $CI_REGISTRY_IMAGE/dev/node-organization:${CI_COMMIT_REF_SLUG}
    entrypoint: [""]

.adminjob:
  image:
    name: $CI_REGISTRY_IMAGE/dev/node-admin:${CI_COMMIT_REF_SLUG}
    entrypoint: [""]

.clientjob:
  image:
    name: $CI_REGISTRY_IMAGE/dev/node-admin:${CI_COMMIT_REF_SLUG}
    entrypoint: [""]

# Stage SECURITY
## Group SECURITY VULNERABILITIES
## Job SECURITY VULNERABILITIES SERVER
security:vulnerabilities:server:
  extends: .serverjob
  stage: security
  allow_failure: true
  script:
    - cd /var/www
    - local-php-security-checker

.security-node-vulnerabilities:
  stage: security
  script:
    - cd /var/www
    - yarn audit

# Job SECURITY VULNERABILITIES ORGANIZATION
security:vulnerabilities:organization:
  extends:
    - .organizationjob
    - .security-node-vulnerabilities
  allow_failure: true

# Job SECURITY VULNERABILITIES ADMIN
security:vulnerabilities:admin:
  extends:
    - .adminjob
    - .security-node-vulnerabilities
  allow_failure: true

# Job SECURITY VULNERABILITIES CLIENT
security:vulnerabilities:client:
  extends:
    - .clientjob
    - .security-node-vulnerabilities
  allow_failure: true

# Stage QUALITY
## Group QUALITY LINT
## Job QUALITY LINT SERVER
quality:lint:server:
  extends: .serverjob
  stage: quality
  script:
    - cd /var/www
    - php vendor/bin/php-cs-fixer fix --dry-run --verbose

.quality-node-lint:
  stage: quality
  script:
    - cd /var/www
    - yarn lint

## Job QUALITY LINT ORGANIZATION
quality:lint:organization:
  extends:
    - .organizationjob
    - .quality-node-lint

## Job QUALITY LINT ADMIN
quality:lint:admin:
  extends:
    - .adminjob
    - .quality-node-lint

## Job QUALITY LINT CLIENT
quality:lint:client:
  extends:
    - .clientjob
    - .quality-node-lint

## Job QUALITY STATIC ANALYSIS SERVER
quality:static-analysis:server:
  extends: .serverjob
  stage: quality
  script:
    - cd /var/www
    - php vendor/bin/phpstan analyse src -l 5 --no-progress

# Stage TEST
## Group TEST SERVER
.test-server:
  stage: test
  services:
    - name: mariadb:10.5.2
      alias: mariadb
  variables:
    # server env
    APP_ENV: test
    APP_DEBUG: 0
    DATABASE_URL: mysql://myeducation_user:myeducation_password@mariadb:3306/myeducation_database
    JWT_COOKIE_HP: myeducation_jwt_hp
    JWT_COOKIE_SIGNATURE: myeducation_jwt_signature
    DOMAIN: myeducation.ci
    # mariadb env
    MYSQL_ROOT_PASSWORD: myeducation_root
    MYSQL_DATABASE: myeducation_database
    MYSQL_USER: myeducation_user
    MYSQL_PASSWORD: myeducation_password

## Job TEST SERVER
test:server:
  extends:
    - .serverjob
    - .test-server
  script:
    - cd /var/www
    # Generate RSA key pair for jwt
    - openssl genrsa -out /var/www/config/jwt/private.pem 4096
    - openssl pkey -in /var/www/config/jwt/private.pem -out /var/www/config/jwt/public.pem -pubout
    # run migrations
    - php bin/console doctrine:migration:migrate -n
    # enable coverage
    - docker-php-ext-enable pcov
    # run tests
    - vendor/bin/codecept run --xml ${CI_PROJECT_DIR}/report.xml --coverage --coverage-xml --coverage-text --coverage-cobertura
    # Copy cobertura.xml file on the build directory to be uploaded as artifact
    - cp tests/_output/cobertura.xml ${CI_PROJECT_DIR}/cobertura.xml
    # change the filename attribute so Gitlab could match files
    - sed -i 's filename=" filename="src/ g' ${CI_PROJECT_DIR}/cobertura.xml
    # Show Coverage for CI
    - COVERAGE_PERCENT_FULL=$(grep -oP "^\s*Lines:\s*([0-9]+)(\.[0-9]+)?" tests/_output/coverage.txt | grep -oP "([0-9]+)(\.[0-9]+)?")
    - echo "Coverage $COVERAGE_PERCENT_FULL%"
    # Ensure we are >= 65% code coverage
    - COVERAGE_PERCENT=$(grep -oP "^\s*Lines:\s*([0-9]+)" tests/_output/coverage.txt | grep -oP "([0-9]+)")
#    - test "$COVERAGE_PERCENT" -ge 65
  artifacts:
    reports:
      junit: report.xml
      cobertura: cobertura.xml

## Job TEST SERVER MIGRATIONS
test:server:migrations:
  extends:
    - .serverjob
    - .test-server
  script:
    - cd /var/www
    # run migrations
    - php bin/console doctrine:migration:migrate -n
    # Add fixtures
    - php bin/console hautelook:fixtures:load --purge-with-truncate -n
    # rollback 5 migrations
    - php bin/console doctrine:migration:migrate prev -n
    - php bin/console doctrine:migration:migrate prev -n
    - php bin/console doctrine:migration:migrate prev -n
    - php bin/console doctrine:migration:migrate prev -n
    - php bin/console doctrine:migration:migrate prev -n
    # relaunch them
    - php bin/console doctrine:migration:migrate -n

## Job TEST SERVER SCHEMA
test:server:schema:
  extends:
    - .serverjob
    - .test-server
  script:
    - cd /var/www
    - php bin/console doctrine:migration:migrate -n
    - php bin/console doctrine:schema:validate

.test-node:
  stage: test
  script:
    - /setup-env.sh /var/www/public
    - cd /var/www
    - yarn test:unit
    - yarn test:e2e:headless

## Job TEST ORGANIZATION
test:organization:
  extends:
   - .organizationjob
   - .test-node

## Job TEST ADMIN
test:admin:
  extends:
    - .adminjob
    - .test-node

## Job TEST CLIENT
test:client:
  extends:
    - .clientjob
    - .test-node

deploy:
  stage: deploy
  image: alpine
  variables:
    ANSIBLE_HOST_KEY_CHECKING: 'False'
    GIT_STRATEGY: fetch
  before_script:
    # add openssh
    - apk add --update openssh ansible
    # Run ssh-agent (inside the build environment)
    - eval $(ssh-agent -s)
    # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
    - echo "$DEPLOY_SSH_PRIVATE_KEY" >/tmp/deploy.key
    - chmod 600 /tmp/deploy.key
    - ssh-add /tmp/deploy.key
  script:
    - echo "$ANSIBLE_VAULT_PASSWORD" > ansible/.vault
    - >-
      ansible-playbook
      --private-key /tmp/deploy.key
      --vault-password-file ansible/.vault
      -e "APP_VERSION=$CI_COMMIT_REF_SLUG DEPLOY_REGISTRY_TOKEN=$DEPLOY_REGISTRY_TOKEN"
      -i ansible/inventories/hosts.yml
      ansible/deploy.yml
#  rules:
#    - if: '$CI_COMMIT_TAG'
  when: manual
