<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Model\Form;

use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordRequestFormModel
{
    /**
     * @Assert\Email
     */
    public ?string $email;

    public function getEmail(): ?string
    {
        return $this->email;
    }
}
