<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\CourseUnitRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseUnitRepository::class)
 * @ORM\Table(name="course_units")
 */
class CourseUnit extends Unit
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=PathUnit::class, inversedBy="courseUnits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pathUnit;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPathUnit(): ?PathUnit
    {
        return $this->pathUnit;
    }

    public function setPathUnit(?PathUnit $pathUnit): self
    {
        $this->pathUnit = $pathUnit;

        return $this;
    }
}
