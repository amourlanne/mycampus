<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`groups`", uniqueConstraints={@ORM\UniqueConstraint(name="group_slug", columns={"slug", "session_id"})})
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"group_list", "group_read", "group_create", "student_list", "teaching_list", "test_list", "test_read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=65)
     * @Groups({"group_list", "group_read", "group_create", "student_list", "teaching_list"})
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity=Session::class, inversedBy="groups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    /**
     * @ORM\ManyToMany(targetEntity=Student::class, inversedBy="groups")
     * @ORM\JoinTable(name="groups_students")
     */
    private $students;

    /**
     * @ORM\ManyToOne(targetEntity=Path::class, inversedBy="groups")
     * @Groups({"group_list_path", "group_read_path"})
     */
    private $path;

    /**
     * @ORM\OneToMany(targetEntity=Teaching::class, mappedBy="group", orphanRemoval=true)
     * @Groups({"group_teaching"})
     */
    private $teachings;

    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->teachings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    /**
     * @Groups({"group_list"})
     */
    public function getStudentCount(): int
    {
        return $this->getStudents()->count();
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->contains($student)) {
            $this->students->removeElement($student);
        }

        return $this;
    }

    public function getPath(): ?Path
    {
        return $this->path;
    }

    public function setPath(?Path $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Collection|Teaching[]
     */
    public function getTeachings(): Collection
    {
        return $this->teachings;
    }

    public function addTeaching(Teaching $teaching): self
    {
        if (!$this->teachings->contains($teaching)) {
            $this->teachings[] = $teaching;
            $teaching->setGroup($this);
        }

        return $this;
    }

    public function removeTeaching(Teaching $teaching): self
    {
        if ($this->teachings->contains($teaching)) {
            $this->teachings->removeElement($teaching);
            // set the owning side to null (unless already changed)
            if ($teaching->getGroup() === $this) {
                $teaching->setGroup(null);
            }
        }

        return $this;
    }
}
