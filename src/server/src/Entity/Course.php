<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CourseRepository::class)
 * @ORM\Table(name="courses")
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"course_list", "course_read", "teaching_list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"course_list", "course_read", "course_create", "path_read", "teaching_list", "test_list", "test_read", "group_teaching"})
     */
    private $name;

//    /**
//     * @ORM\ManyToOne(targetEntity=Path::class, inversedBy="courses")
//     * @ORM\JoinColumn(onDelete="SET NULL")
//     * @Groups({"course_list", "course_read"})
//     */
//    private $path;

    /**
     * @ORM\OneToMany(targetEntity=Teaching::class, mappedBy="course", orphanRemoval=true)
     */
    private $teachings;

    /**
     * @ORM\ManyToOne(targetEntity=Session::class, inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    /**
     * @ORM\ManyToOne(targetEntity=SessionPart::class, inversedBy="courses")
     */
    private $sessionPart;

    /**
     * @ORM\ManyToOne(targetEntity=Unit::class, inversedBy="courses")
     * @ORM\JoinColumn(nullable=false)
     */
    private $unit;

    public function __construct()
    {
        $this->teachings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Teaching[]
     */
    public function getTeachings(): Collection
    {
        return $this->teachings;
    }

    public function addTeaching(Teaching $teaching): self
    {
        if (!$this->teachings->contains($teaching)) {
            $this->teachings[] = $teaching;
            $teaching->setCourse($this);
        }

        return $this;
    }

    public function removeTeaching(Teaching $teaching): self
    {
        if ($this->teachings->contains($teaching)) {
            $this->teachings->removeElement($teaching);
            // set the owning side to null (unless already changed)
            if ($teaching->getCourse() === $this) {
                $teaching->setCourse(null);
            }
        }

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getSessionPart(): ?SessionPart
    {
        return $this->sessionPart;
    }

    public function setSessionPart(?SessionPart $sessionPart): self
    {
        $this->sessionPart = $sessionPart;

        return $this;
    }

    public function getUnit(): ?Unit
    {
        return $this->unit;
    }

    public function setUnit(?Unit $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @Groups({"course_list_path", "course_read_path"})
     */
    public function getPath(): ?Path
    {
        if ($this->unit instanceof CourseUnit) {
            return $this->unit->getPathUnit()->getPath();
        } elseif ($this->unit instanceof PathUnit) {
            return $this->unit->getPath();
        }

        return $this->unit;
    }
}
