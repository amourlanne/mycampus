<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\TrainingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TrainingRepository::class)
 * @ORM\Table(name="trainings", uniqueConstraints={@ORM\UniqueConstraint(name="training_slug", columns={"organization_id","slug"})})
 */
class Training
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"training_read", "session_r_training"})
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=65)
     * @Groups({"training_read"})
     */
    private ?string $slug;

    /**
     * @ORM\ManyToOne(targetEntity=Organization::class, inversedBy="trainings")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Organization $organization;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="training", orphanRemoval=true)
     * @Groups({"training_session"})
     */
    private $sessions;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

//    /**
//     * @return Collection|Course[]
//     */
//    public function getCourses(): Collection
//    {
//        return $this->courses;
//    }
//
//    public function addCourse(Course $course): self
//    {
//        if (!$this->courses->contains($course)) {
//            $this->courses[] = $course;
//            $course->setTraining($this);
//        }
//
//        return $this;
//    }
//
//    public function removeCourse(Course $course): self
//    {
//        if ($this->courses->contains($course)) {
//            $this->courses->removeElement($course);
//            // set the owning side to null (unless already changed)
//            if ($course->getTraining() === $this) {
//                $course->setTraining(null);
//            }
//        }
//
//        return $this;
//    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addTrainingSession(Session $trainingSession): self
    {
        if (!$this->sessions->contains($trainingSession)) {
            $this->sessions[] = $trainingSession;
            $trainingSession->setTraining($this);
        }

        return $this;
    }

    public function removeTrainingSession(Session $trainingSession): self
    {
        if ($this->sessions->contains($trainingSession)) {
            $this->sessions->removeElement($trainingSession);
            // set the owning side to null (unless already changed)
            if ($trainingSession->getTraining() === $this) {
                $trainingSession->setTraining(null);
            }
        }

        return $this;
    }
}
