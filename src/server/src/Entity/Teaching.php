<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\TeachingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TeachingRepository::class)
 * @ORM\Table(name="teachings", uniqueConstraints={@ORM\UniqueConstraint(name="teaching_group_course", columns={"group_id", "course_id"})})
 */
class Teaching
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"teaching_list", "teaching_read", "test_list", "group_teaching"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Group::class, inversedBy="teachings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"teaching_list", "test_list", "test_read"})
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity=Course::class, inversedBy="teachings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"teaching_list", "test_list", "test_read", "group_teaching"})
     */
    private ?Course $course;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="teachings")
     * @Groups({"teaching_list", "teaching_read", "group_teaching"})
     */
    private $professor;

    /**
     * @ORM\OneToMany(targetEntity=Test::class, mappedBy="teaching", orphanRemoval=true)
     */
    private $tests;

    public function __construct()
    {
        $this->tests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroup(): ?Group
    {
        return $this->group;
    }

    public function setGroup(?Group $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getProfessor(): ?Professor
    {
        return $this->professor;
    }

    public function setProfessor(?Professor $professor): self
    {
        $this->professor = $professor;

        return $this;
    }

    /**
     * @return Collection|Test[]
     */
    public function getTests(): Collection
    {
        return $this->tests;
    }

    public function addTest(Test $test): self
    {
        if (!$this->tests->contains($test)) {
            $this->tests[] = $test;
            $test->setTeaching($this);
        }

        return $this;
    }

    public function removeTest(Test $test): self
    {
        if ($this->tests->contains($test)) {
            $this->tests->removeElement($test);
            // set the owning side to null (unless already changed)
            if ($test->getTeaching() === $this) {
                $test->setTeaching(null);
            }
        }

        return $this;
    }
}
