<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\SessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SessionRepository::class)
 * @ORM\Table(name="sessions", uniqueConstraints={@ORM\UniqueConstraint(name="session_training_year", columns={"training_id", "school_year"})})
 */
class Session
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"training_session", "session_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Training::class, inversedBy="sessions")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Groups({"session_r_training"})
     */
    private $training;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"training_session", "session_list", "session_read"})
     */
    private $schoolYear;

    /**
     * @ORM\OneToMany(targetEntity=Course::class, mappedBy="session", orphanRemoval=true)
     */
    private $courses;

    /**
     * @ORM\OneToMany(targetEntity=Path::class, mappedBy="session", orphanRemoval=true)
     */
    private $paths;

    /**
     * @ORM\OneToMany(targetEntity=Group::class, mappedBy="session", orphanRemoval=true)
     */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity=PathGroup::class, mappedBy="session", orphanRemoval=true)
     */
    private $pathGroups;

    /**
     * @ORM\OneToMany(targetEntity=SessionPart::class, mappedBy="session", orphanRemoval=true)
     */
    private $sessionParts;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Choice(callback="getUnitLevels")
     * @Serializer\Groups({"session_read", "session_create"})
     */
    private $unitLevel = 1;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
        $this->paths = new ArrayCollection();
        $this->pathGroups = new ArrayCollection();
        $this->sessionParts = new ArrayCollection();
    }

    public static function getUnitLevels()
    {
        return [1, 2, 3];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTraining(): ?Training
    {
        return $this->training;
    }

    public function setTraining(?Training $training): self
    {
        $this->training = $training;

        return $this;
    }

    public function getSchoolYear(): ?string
    {
        return $this->schoolYear;
    }

    public function setSchoolYear(string $schoolYear): self
    {
        $this->schoolYear = $schoolYear;

        return $this;
    }

    /**
     * @return Collection|Path[]
     */
    public function getPaths(): Collection
    {
        return $this->paths;
    }

    public function addPath(Path $path): self
    {
        if (!$this->paths->contains($path)) {
            $this->paths[] = $path;
            $path->setSession($this);
        }

        return $this;
    }

    public function removePath(Path $path): self
    {
        if ($this->paths->contains($path)) {
            $this->paths->removeElement($path);
            // set the owning side to null (unless already changed)
            if ($path->getSession() === $this) {
                $path->setSession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->setSession($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->contains($course)) {
            $this->courses->removeElement($course);
            // set the owning side to null (unless already changed)
            if ($course->getSession() === $this) {
                $course->setSession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->setSession($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
            // set the owning side to null (unless already changed)
            if ($group->getSession() === $this) {
                $group->setSession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PathGroup[]
     */
    public function getPathGroups(): Collection
    {
        return $this->pathGroups;
    }

    public function addPathGroup(PathGroup $pathGroup): self
    {
        if (!$this->pathGroups->contains($pathGroup)) {
            $this->pathGroups[] = $pathGroup;
            $pathGroup->setSession($this);
        }

        return $this;
    }

    public function removePathGroup(PathGroup $pathGroup): self
    {
        if ($this->pathGroups->contains($pathGroup)) {
            $this->pathGroups->removeElement($pathGroup);
            // set the owning side to null (unless already changed)
            if ($pathGroup->getSession() === $this) {
                $pathGroup->setSession(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SessionPart[]
     */
    public function getSessionParts(): Collection
    {
        return $this->sessionParts;
    }

    public function addSessionPart(SessionPart $sessionPart): self
    {
        if (!$this->sessionParts->contains($sessionPart)) {
            $this->sessionParts[] = $sessionPart;
            $sessionPart->setSession($this);
        }

        return $this;
    }

    public function removeSessionPart(SessionPart $sessionPart): self
    {
        if ($this->sessionParts->contains($sessionPart)) {
            $this->sessionParts->removeElement($sessionPart);
            // set the owning side to null (unless already changed)
            if ($sessionPart->getSession() === $this) {
                $sessionPart->setSession(null);
            }
        }

        return $this;
    }

    public function getUnitLevel(): ?int
    {
        return $this->unitLevel;
    }

    public function setUnitLevel(int $unitLevel): self
    {
        $this->unitLevel = $unitLevel;

        return $this;
    }
}
