<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;

class OrganizationLoginRequestMatcher extends OrganizationSlugHeaderRequestMatcher
{
    public function matches(Request $request)
    {
        return parent::matches($request) && '/login' === $request->getRequestUri();
    }
}
