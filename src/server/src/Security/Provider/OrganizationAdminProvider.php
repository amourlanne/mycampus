<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Security\Provider;

use App\Entity\OrganizationAdmin;

class OrganizationAdminProvider extends OrganizationUserProvider
{
    public function getUserClass(): string
    {
        return OrganizationAdmin::class;
    }
}
