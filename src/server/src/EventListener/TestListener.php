<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\EventListener;

use App\Entity\Mark;
use App\Entity\Test;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class TestListener implements EventSubscriber
{
    private EntityManagerInterface $entityManager;

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
        ];
    }

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Test) {
            return;
        }

        foreach ($entity->getTeaching()->getGroup()->getStudents() as $student) {
            $mark = new Mark();
            $mark->setStudent($student);
            $mark->setTest($entity);

            $this->entityManager->persist($mark);
        }
        $this->entityManager->flush();
    }
}
