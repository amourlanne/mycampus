<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\CourseUnit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseUnit|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseUnit|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseUnit[]    findAll()
 * @method CourseUnit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseUnitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseUnit::class);
    }

    // /**
    //  * @return CourseUnit[] Returns an array of CourseUnit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseUnit
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
