<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\SessionPart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SessionPart|null find($id, $lockMode = null, $lockVersion = null)
 * @method SessionPart|null findOneBy(array $criteria, array $orderBy = null)
 * @method SessionPart[]    findAll()
 * @method SessionPart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionPartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SessionPart::class);
    }

    // /**
    //  * @return SessionPart[] Returns an array of SessionPart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SessionPart
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
