<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Professor;

use App\Entity\Professor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ProfessorSessionController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/professor/sessions",
     *     name="get_professor_sessions",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getSessionList(Request $request): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof Professor) {
            throw new NotFoundHttpException('Route not found.');
        }

        $organization = $request->attributes->get('organization');

        return new JsonResponse([], 200, [], true);
    }
}
