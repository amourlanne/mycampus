<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization;

use App\Entity\Organization;
use App\Entity\OrganizationUser;
use App\Entity\User;
use App\Model\Form\ChangePasswordFormModel;
use App\Model\Form\ResetPasswordRequestFormModel;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Controller\ResetPasswordControllerTrait;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;

/**
 * @Route(
 *     "/reset-password",
 *     condition="request.headers.get('X-Organization-Slug')"
 * )
 */
class ResetPasswordController extends AbstractController
{
    use ResetPasswordControllerTrait;

    private ResetPasswordHelperInterface $resetPasswordHelper;
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;

    public function __construct(
        ResetPasswordHelperInterface $resetPasswordHelper,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->resetPasswordHelper = $resetPasswordHelper;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @Route(
     *     "",
     *     name="organization_forgot_password_request",
     *     methods={"POST"}
     * )
     */
    public function request(Request $request, MailerInterface $mailer, string $appDomain): JsonResponse
    {
        /** @var ResetPasswordRequestFormModel $resetPasswordRequestFormData */
        $resetPasswordRequestFormData = $this->serializer->deserialize(
            $request->getContent(),
            ResetPasswordRequestFormModel::class,
            'json'
        );

        $errors = $this->validator->validate($resetPasswordRequestFormData);

        if (count($errors) > 0) {
            /** @var string $errors */
            $errorsString = (string) $errors;
            // TODO: set valid error message
            throw new BadRequestHttpException($errorsString);
        }

        $organization = $request->attributes->get('organization');

        return $this->processSendingPasswordResetEmail(
            $resetPasswordRequestFormData->getEmail(),
            $mailer,
            $organization,
            $appDomain
        );
    }

    /**
     * @Route(
     *     "/reset/{token}",
     *     name="organization_validate_reset_password_token",
     *     methods={"GET"}
     * )
     */
    public function validateToken(Request $request, string $token): JsonResponse
    {
        $organization = $request->attributes->get('organization');
        $this->validateTokenAndFetchUser($token, $organization);

        return new JsonResponse('', 200, [], true);
    }

    /**
     * @Route(
     *     "/reset/{token}",
     *     name="organization_reset_password",
     *     methods={"POST"}
     * )
     */
    public function reset(Request $request, string $token): JsonResponse
    {
        $organization = $request->attributes->get('organization');
        $user = $this->validateTokenAndFetchUser($token, $organization);

        // The token is valid; allow the user to change their password.
        /** @var ChangePasswordFormModel $changePasswordFormData */
        $changePasswordFormData = $this->serializer->deserialize(
            $request->getContent(),
            ChangePasswordFormModel::class,
            'json'
        );

        $errors = $this->validator->validate($changePasswordFormData);

        if (count($errors) > 0) {
            /** @var string $errors */
            $errorsString = (string) $errors;
            // TODO: set valid error message
            throw new BadRequestHttpException($errorsString);
        }

        // A password reset token should be used only once, remove it.
        $this->resetPasswordHelper->removeResetRequest($token);

        $user->setPlainPassword($changePasswordFormData->getPlainPassword());
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse('', 200, [], true);
    }

    private function validateTokenAndFetchUser(string $token, Organization $organization): OrganizationUser
    {
        try {
            $user = $this->resetPasswordHelper->validateTokenAndFetchUser($token);

            if (!$user instanceof OrganizationUser || $user->getOrganization() !== $organization) {
                throw new NotFoundHttpException('There was a problem validating your reset request');
            }

            return $user;
        } catch (ResetPasswordExceptionInterface $e) {
            throw new NotFoundHttpException(sprintf('There was a problem validating your reset request - %s', $e->getReason()));
        }
    }

    private function processSendingPasswordResetEmail(string $emailFormData, MailerInterface $mailer, Organization $organization, string $appDomain): JsonResponse
    {
        $user = $this->getDoctrine()->getRepository(OrganizationUser::class)->findOneBy([
            'email' => $emailFormData,
            'organization' => $organization,
        ]);

        // Do not reveal whether a user account was found or not.
        if (!$user) {
            return new JsonResponse('', 200, [], true);
        }

        try {
            $resetToken = $this->resetPasswordHelper->generateResetToken($user);
        } catch (ResetPasswordExceptionInterface $e) {
            return new JsonResponse('', 200, [], true);
        }

        $email = (new TemplatedEmail())
            ->from(new Address('no-reply@myeducation.local', 'MyEducation'))
            ->to($user->getEmail())
            ->subject('Your password reset request')
            ->htmlTemplate('reset_password/email.html.twig')
            ->context([
                'resetLink' => sprintf('https://%s.%s/reset-password/reset/%s', $organization->getSlug(), $appDomain, $resetToken->getToken()),
                'tokenLifetime' => $this->resetPasswordHelper->getTokenLifetime(),
            ])
        ;

        $mailer->send($email);

        return new JsonResponse('', 200, [], true);
    }
}
