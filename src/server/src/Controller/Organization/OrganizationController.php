<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/organizations",
     *     name="get_current_organization",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getCurrentOrganization(Request $request)
    {
        $organization = $request->attributes->get('organization');

        $organizationData = $this->serializer->serialize($organization, 'json', ['groups' => 'organization_read']);

        return new JsonResponse($organizationData, 200, [], true);
    }
}
