<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Student;

use App\Entity\Mark;
use App\Entity\Organization;
use App\Entity\Session;
use App\Entity\Student;
use App\Repository\MarkRepository;
use App\Repository\SessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class StudentSessionController extends AbstractController
{
    private SessionRepository $sessionRepository;
    private MarkRepository $markRepository;
    private SerializerInterface $serializer;

    public function __construct(SessionRepository $sessionRepository, MarkRepository $markRepository, SerializerInterface $serializer)
    {
        $this->sessionRepository = $sessionRepository;
        $this->markRepository = $markRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/student/sessions",
     *     name="get_student_sessions",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getSessionList(Request $request): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof Student) {
            throw new NotFoundHttpException('Route not found.');
        }

        $organization = $request->attributes->get('organization');

        $sessionList = $this->sessionRepository
            ->createQueryBuilder('s')
            ->innerJoin('s.training', 't')
            ->innerJoin('s.groups', 'g')
            ->innerJoin('g.students', 'st')
            ->where('t.organization = :organizationParam')
            ->andWhere('st = :studentParam')
            ->setParameter('organizationParam', $organization)
            ->setParameter('studentParam', $user)
            ->getQuery()
            ->getResult();

        $sessionListData = $this->serializer->serialize($sessionList, 'json', ['groups' => ['session_list', 'session_training']]);

        return new JsonResponse($sessionListData, 200, [], true);
    }

    /**
     * @Route(
     *     "/student/sessions/{school_year}",
     *     name="get_student_session",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')",
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     */
    public function getSession(Request $request, Organization $organization, string $school_year): JsonResponse
    {
        // TODO: get inscription and not session
        $user = $this->getUser();

        if (!$user instanceof Student) {
            throw new NotFoundHttpException('Route not found.');
        }

        $session = $this->sessionRepository
            ->createQueryBuilder('s')
            ->innerJoin('s.training', 't')
            ->innerJoin('s.groups', 'g')
            ->innerJoin('g.students', 'st')
            ->where('s.schoolYear = :schoolYearParam')
            ->andWhere('t.organization = :organizationParam')
            ->andWhere('st = :studentParam')
            ->setParameter('schoolYearParam', $school_year)
            ->setParameter('organizationParam', $organization)
            ->setParameter('studentParam', $user)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$session) {
            throw new NotFoundHttpException(sprintf('No session found for year %s.', $school_year));
        }

        $sessionData = $this->serializer->serialize($session, 'json', ['groups' => ['session_list', 'session_r_training']]);

        return new JsonResponse($sessionData, 200, [], true);
    }

    /**
     * @Route(
     *     "/student/{school_year}/marks",
     *     name="get_student_session_marks",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')",
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     */
    public function getSessionMarks(Request $request, Organization $organization, string $school_year): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof Student) {
            throw new NotFoundHttpException('Route not found.');
        }

        $session = $this->sessionRepository
            ->createQueryBuilder('s')
            ->innerJoin('s.training', 't')
            ->innerJoin('s.groups', 'g')
            ->innerJoin('g.students', 'st')
            ->where('s.schoolYear = :schoolYearParam')
            ->andWhere('t.organization = :organizationParam')
            ->andWhere('st = :studentParam')
            ->setParameter('schoolYearParam', $school_year)
            ->setParameter('organizationParam', $organization)
            ->setParameter('studentParam', $user)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$session) {
            throw new NotFoundHttpException(sprintf('No session found for year %s.', $school_year));
        }

        /** @var Mark[] $marks */
        $marks = $this->markRepository
            ->createQueryBuilder('m')
            ->where('m.student = :userParam')
            ->innerJoin('m.test', 't')
            ->innerJoin('t.teaching', 'tch')
            ->innerJoin('tch.course', 'c')
            ->andWhere('c.session = :sessionParam')
            ->setParameter('userParam', $user)
            ->setParameter('sessionParam', $session)
            ->getQuery()
            ->getResult();

        $testMarks = [];

        foreach ($marks as $mark) {
            $course = $mark->getTest()->getTeaching()->getCourse();
            if (!isset($testMarks[$course->getId()])) {
                $testMarks[$course->getId()]['name'] = $course->getName();
                $testMarks[$course->getId()]['id'] = $course->getId();
            }
            $testMarks[$course->getId()]['marks'][] = $mark;
        }

        $marksData = $this->serializer->serialize(array_values($testMarks), 'json', ['groups' => ['mark_l', 'mark_l_test']]);

        return new JsonResponse($marksData, 200, [], true);
    }
}
