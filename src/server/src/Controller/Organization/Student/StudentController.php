<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Student;

use App\Entity\Course;
use App\Entity\Group;
use App\Entity\Mark;
use App\Entity\Path;
use App\Entity\Session;
use App\Entity\Student;
use App\Entity\Teaching;
use App\Entity\Training;
use App\Repository\CourseRepository;
use App\Repository\MarkRepository;
use App\Repository\SessionRepository;
use App\Repository\TeachingRepository;
use App\Repository\TrainingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class StudentController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/student/{school_year}/teachings",
     *     name="get_student_teachings",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getStudentSessionTeachings(Request $request, string $school_year): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof Student) {
            throw new NotFoundHttpException('Route not found.');
        }

        $organization = $request->attributes->get('organization');

        /** @var SessionRepository $sessionRepository */
        $sessionRepository = $this->entityManager
            ->getRepository(Session::class);

        $session = $sessionRepository
            ->createQueryBuilder('s')
            ->innerJoin('s.training', 't')
            ->innerJoin('s.groups', 'g')
            ->innerJoin('g.students', 'st')
            ->where('s.schoolYear = :schoolYearParam')
            ->andWhere('t.organization = :organizationParam')
            ->andWhere('st = :studentParam')
            ->setParameter('schoolYearParam', $school_year)
            ->setParameter('organizationParam', $organization)
            ->setParameter('studentParam', $user)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$session) {
            throw new NotFoundHttpException(sprintf('No session found for year %s.', $school_year));
        }

        /** @var SessionRepository $sessionRepository */
        $sessionRepository = $this->entityManager
            ->getRepository(Group::class);

        $groupList = $sessionRepository
            ->createQueryBuilder('g')
            ->innerJoin('g.students', 's')
            ->where('s = :studentParam')
            ->andWhere('g.session = :sessionParam')
            ->andWhere('g.path is not null')
            ->setParameter('studentParam', $user)
            ->setParameter('sessionParam', $session)
            ->getQuery()
            ->getResult();

        $groupListData = $this->serializer
            ->serialize($groupList, 'json', ['groups' => ['group_read', 'group_teaching', 'user_list']]);

        return new JsonResponse($groupListData, 200, [], true);
    }

    /**
     * @Route(
     *     "/student/{school_year}/courses",
     *     name="get_student_courses",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getStudentSessionCourses(Request $request, string $school_year): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof Student) {
            throw new NotFoundHttpException('Route not found.');
        }

        $organization = $request->attributes->get('organization');

        /** @var SessionRepository $courseRepository */
        $courseRepository = $this->entityManager
            ->getRepository(Session::class);

        $session = $courseRepository
            ->createQueryBuilder('s')
            ->innerJoin('s.training', 't')
            ->innerJoin('s.groups', 'g')
            ->innerJoin('g.students', 'st')
            ->where('s.schoolYear = :schoolYearParam')
            ->andWhere('t.organization = :organizationParam')
            ->andWhere('st = :studentParam')
            ->setParameter('schoolYearParam', $school_year)
            ->setParameter('organizationParam', $organization)
            ->setParameter('studentParam', $user)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$session) {
            throw new NotFoundHttpException(sprintf('No session found for year %s.', $school_year));
        }

        /** @var CourseRepository $courseRepository */
        $courseRepository = $this->entityManager
            ->getRepository(Course::class);

        $courseList = $courseRepository
            ->createQueryBuilder('c')
            ->innerJoin(Path::class, 'p', 'WITH', 'c.unit = p.id')
            ->innerJoin('p.groups', 'g')
            ->innerJoin('g.students', 's')
            ->where('s = :studentParam')
            ->andWhere('g.session = :sessionParam')
            ->setParameter('studentParam', $user)
            ->setParameter('sessionParam', $session)
            ->getQuery()
            ->getResult();

        $courseListData = $this->serializer
            ->serialize($courseList, 'json', ['groups' => ['course_list']]);

        return new JsonResponse($courseListData, 200, [], true);
    }

    /**
     * @Route(
     *     "/student/teachings/{teaching_id}",
     *     name="get_student_teaching",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getStudentTeaching(Request $request, string $teaching_id): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof Student) {
            throw new NotFoundHttpException('Route not found.');
        }

        $organization = $request->attributes->get('organization');

        /** @var TrainingRepository $trainingRepository */
        $trainingRepository = $this->entityManager
            ->getRepository(Training::class);

        $currentTraining = $trainingRepository
            ->createQueryBuilder('t')
            ->where('t.organization = :organizationParam')
            ->setParameter(':organizationParam', $organization)
            ->innerJoin('t.sessions', 'ts')
            ->innerJoin('ts.groups', 'g')
            ->innerJoin('g.students', 's')
            ->andWhere('s = :studentParam')
            ->setParameter(':studentParam', $user) // and where period
            ->getQuery()
            ->getOneOrNullResult();

        if (!$currentTraining) {
            throw new NotFoundHttpException('No training found.');
        }

        /** @var TrainingRepository $teachingRepository */
        $teachingRepository = $this->entityManager
            ->getRepository(Teaching::class);

        $teaching = $teachingRepository
            ->createQueryBuilder('tch')
            ->where('tch.id = :teachingIdParam')
            ->innerJoin('tch.group', 'g')
            ->innerJoin('g.session', 's')
            ->innerJoin('s.training', 't')
            ->andWhere('t = :trainingParam')
            ->setParameter(':teachingIdParam', $teaching_id)
            ->setParameter(':trainingParam', $currentTraining)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$teaching) {
            throw new NotFoundHttpException('No teaching found for id '.$teaching_id);
        }

        $teachingData = $this->serializer
            ->serialize($teaching, 'json', ['groups' => ['group_teaching', 'user_list']]);

        return new JsonResponse($teachingData, 200, [], true);
    }

    /**
     * @Route(
     *     "/student/teachings/{teaching_id}/tests",
     *     name="get_student_teaching_tests",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getStudentTeachingTests(Request $request, string $teaching_id): JsonResponse
    {
        $user = $this->getUser();

        if (!$user instanceof Student) {
            throw new NotFoundHttpException('Route not found.');
        }

        $organization = $request->attributes->get('organization');

        /** @var TrainingRepository $trainingRepository */
        $trainingRepository = $this->entityManager
            ->getRepository(Training::class);

        $currentTraining = $trainingRepository
            ->createQueryBuilder('t')
            ->where('t.organization = :organizationParam')
            ->setParameter(':organizationParam', $organization)
            ->innerJoin('t.sessions', 'ts')
            ->innerJoin('ts.groups', 'g')
            ->innerJoin('g.students', 's')
            ->andWhere('s = :studentParam')
            ->setParameter(':studentParam', $user) // and where period
            ->getQuery()
            ->getOneOrNullResult();

        if (!$currentTraining) {
            throw new NotFoundHttpException('No training found.');
        }

        /** @var TeachingRepository $teachingRepository */
        $teachingRepository = $this->entityManager
            ->getRepository(Teaching::class);

        $teaching = $teachingRepository
            ->createQueryBuilder('tch')
            ->where('tch.id = :teachingIdParam')
            ->innerJoin('tch.group', 'g')
            ->innerJoin('g.session', 'gs')
            ->innerJoin('gs.training', 't')
            ->andWhere('t = :trainingParam')
            ->setParameter(':teachingIdParam', $teaching_id)
            ->setParameter(':trainingParam', $currentTraining)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$teaching) {
            throw new NotFoundHttpException('No teaching found for id '.$teaching_id);
        }

        /** @var MarkRepository $markRepository */
        $markRepository = $this->entityManager
            ->getRepository(Mark::class);

        $marks = $markRepository
            ->createQueryBuilder('m')
            ->innerJoin('m.test', 't')
            ->innerJoin('t.teaching', 'tch')
            ->where('tch.id = :teachingIdParam')
            ->andWhere('m.student = :studentParam')
            ->setParameter(':teachingIdParam', $teaching_id)
            ->setParameter(':studentParam', $user)
            ->getQuery()
            ->getResult()
        ;

        $marksData = $this->serializer
            ->serialize($marks, 'json', ['groups' => ['mark_test_read']]);

        return new JsonResponse($marksData, 200, [], true);
    }
}
