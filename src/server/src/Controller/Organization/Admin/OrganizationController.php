<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Admin;

use App\Entity\Organization;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OpenApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization")
     * @Route(
     *     "/organization-admin/organizations",
     *     name="organization_admin_update_organization",
     *     methods={"PUT"},
     * )
     */
    public function updateOrganization(Request $request, Organization $organization): JsonResponse
    {
        $organization = $this->serializer->deserialize(
            $request->getContent(),
            Organization::class,
            'json',
            [
                'groups' => ['organization_post'],
                'object_to_populate' => $organization,
            ]);

        $this->entityManager->persist($organization);
        $this->entityManager->flush();

        $organisationData = $this->serializer->serialize($organization, 'json', ['groups' => ['organization_read']]);

        return new JsonResponse($organisationData, 200, [], true);
    }
}
