<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Admin\Training\Course;

use App\Entity\Course;
use App\Entity\Group;
use App\Entity\Organization;
use App\Entity\Path;
use App\Entity\Professor;
use App\Entity\Session;
use App\Entity\Teaching;
use App\Entity\Training;
use App\Entity\Unit;
use App\Repository\CourseRepository;
use App\Repository\GroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingCourseController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    private CourseRepository $courseRepository;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses",
     *     name="organization_admin_get_organization_training_course_list",
     *     methods={"GET"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingCourseList(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var CourseRepository $courseRepository */
        $courseRepository = $this->entityManager
            ->getRepository(Course::class);

        $courseListQueryBuilder = $courseRepository
            ->createQueryBuilder('c')
            ->where('c.session = :sessionParam')
            ->setParameter(':sessionParam', $session);

        $courseListQuery = $courseListQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $courseListQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $courseListPaginator = new Paginator($courseListQuery);

        $totalItems = $courseListPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $courseListPaginatorData = [
            'items' => $courseListPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $courseListData = $this->serializer->serialize($courseListPaginatorData, 'json', ['groups' => ['course_list', 'course_list_path']]);

        return new JsonResponse($courseListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}",
     *     name="organization_admin_get_organization_training_course",
     *     methods={"GET"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     */
    public function getOrganizationTrainingCourse(Organization $organization, Training $training, Session $session, Course $course): JsonResponse
    {
        $courseData = $this->serializer->serialize($course, 'json', ['groups' => ['course_read', 'course_read_path']]);

        return new JsonResponse($courseData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses",
     *     name="organization_admin_create_organization_training_course",
     *     methods={"POST"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function createOrganizationTrainingCourse(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var Course $course */
        $course = $this->serializer->deserialize(
            $request->getContent(),
            Course::class,
            'json',
            [
                'groups' => ['course_create'],
            ]);

        $content = $request->getContent();
        $contentObject = json_decode($content);

        if (property_exists($contentObject, 'unit')) {
            switch (gettype($contentObject->unit)) {
                case 'NULL':
                    $course->setUnit(null);
                    break;
                case 'integer':
                    $unitId = $contentObject->unit;

                    $unit = $this->entityManager
                        ->getRepository(Path::class)
                        ->findOneBy([
                            'id' => $unitId,
                            'session' => $session,
                        ]);

                    if (!$unit) {
                        throw new NotFoundHttpException('No unit found for id '.$unitId);
                    }

                    $course->setUnit($unit);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong unit id.');
            }
        }

        $course->setSession($session);

        $this->entityManager->persist($course);
        $this->entityManager->flush();

        $courseData = $this->serializer->serialize($course, 'json', ['groups' => ['course_read', 'course_read_path']]);

        return new JsonResponse($courseData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}",
     *     name="organization_admin_delete_organization_training_course",
     *     methods={"DELETE"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     */
    public function deleteOrganizationTrainingCourse(Request $request, Organization $organization, Training $training, Session $session, Course $course): JsonResponse
    {
        $this->entityManager->remove($course);
        $this->entityManager->flush();

        return new JsonResponse('', 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}",
     *     name="organization_admin_update_organization_training_course",
     *     methods={"PUT"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     */
    public function updateOrganizationTrainingCourse(Request $request, Organization $organization, Training $training, Session $session, Course $course): JsonResponse
    {
        $content = $request->getContent();
        $contentObject = json_decode($content);

        /** @var Course $course */
        $course = $this->serializer->deserialize(
            $request->getContent(),
            Course::class,
            'json',
            [
                'groups' => ['course_create'],
                'object_to_populate' => $course,
            ]);

        if (property_exists($contentObject, 'unit')) {
            switch (gettype($contentObject->unit)) {
                case 'NULL':
                    $course->setUnit(null);
                    break;
                case 'integer':
                    $unitId = $contentObject->unit;

                    /** @var Unit|null $unit */
                    $unit = $this->entityManager
                        ->getRepository(Path::class)
                        ->findOneBy([
                            'id' => $unitId,
                            'session' => $session,
                        ]);

                    if (!$unit) {
                        throw new NotFoundHttpException('No unit found for id '.$unitId);
                    }

                    $course->setUnit($unit);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong unit id.');
            }
        }

        $this->entityManager->persist($course);
        $this->entityManager->flush();

        $courseData = $this->serializer->serialize($course, 'json', ['groups' => ['course_read', 'course_read_path']]);

        return new JsonResponse($courseData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}/teachings/{group_slug}",
     *     name="organization_admin_create_organization_training_course_teaching",
     *     methods={"POST"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "session": "session"}})
     */
    public function startOrganizationTrainingCourseTeaching(Request $request, Organization $organization, Training $training, Session $session, Course $course, Group $group): JsonResponse
    {
        if (null === $group->getPath()) {
            throw new BadRequestHttpException('Group has no path.');
        }

        if (!$group->getPath()->getCourses()->contains($course)) {
            throw new BadRequestHttpException('Course is not attach to the group path.');
        }

        $teaching = new Teaching();
        $teaching->setGroup($group);
        $teaching->setCourse($course);

        $content = $request->getContent();
        $contentObject = json_decode($content);

        if (property_exists($contentObject, 'professor')) {
            switch (gettype($contentObject->professor)) {
                case 'NULL':
                    $teaching->setProfessor(null);
                    break;
                case 'string':
                    $professorEmail = $contentObject->professor;

                    /** @var Professor|null $professor */
                    $professor = $this->entityManager
                        ->getRepository(Professor::class)
                        ->findOneBy([
                            'email' => $professorEmail,
                            'organization' => $organization,
                        ]);

                    if (!$professor) {
                        throw new NotFoundHttpException('No professor found for email '.$professorEmail);
                    }

                    $teaching->setProfessor($professor);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong professor email.');
            }
        }

        $this->entityManager->persist($teaching);
        $this->entityManager->flush();

        $teachingData = $this->serializer->serialize($teaching, 'json', ['groups' => ['teaching_read']]);

        return new JsonResponse($teachingData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}/teachings/{group_slug}",
     *     name="organization_admin_update_organization_training_course_teaching",
     *     methods={"PUT"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "training": "training"}})
     * @ParamConverter("teaching", options={"mapping": {"group": "group", "course": "course"}})
     */
    public function updateOrganizationTrainingCourseTeaching(Request $request, Organization $organization, Training $training, Course $course, Session $session, Group $group, Teaching $teaching): JsonResponse
    {
        $content = $request->getContent();
        $contentObject = json_decode($content);

        if (property_exists($contentObject, 'professor')) {
            switch (gettype($contentObject->professor)) {
                case 'NULL':
                    $teaching->setProfessor(null);
                    break;
                case 'string':
                    $professorEmail = $contentObject->professor;

                    $professor = $this->entityManager
                        ->getRepository(Professor::class)
                        ->findOneBy([
                            'email' => $professorEmail,
                            'organization' => $organization,
                        ]);

                    if (!$professor) {
                        throw new NotFoundHttpException('No professor found for email '.$professorEmail);
                    }

                    $teaching->setProfessor($professor);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong professor email.');
            }
        }

        $this->entityManager->persist($teaching);
        $this->entityManager->flush();

        $teachingData = $this->serializer->serialize($teaching, 'json', ['groups' => ['teaching_list', 'user_list']]);

        return new JsonResponse($teachingData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}/teachings/{group_slug}",
     *     name="organization_admin_get_organization_training_course_teaching",
     *     methods={"GET"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "training": "training"}})
     * @ParamConverter("teaching", options={"mapping": {"group": "group", "course": "course"}})
     */
    public function getOrganizationTrainingCourseTeaching(Request $request, Organization $organization, Training $training, Course $course, Session $session, Group $group, Teaching $teaching): JsonResponse
    {
        $teachingData = $this->serializer->serialize($teaching, 'json', ['groups' => ['teaching_list', 'user_list']]);

        return new JsonResponse($teachingData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}/available-groups",
     *     name="organization_admin_get_organization_training_course_available_group_list",
     *     methods={"GET"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     */
    public function getOrganizationTrainingCourseAvailableGroupList(Request $request, Organization $organization, Training $training, Session $session, Course $course): JsonResponse
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager
            ->getRepository(Group::class);

        $notAvailableGroupListQueryBuilder = $groupRepository
            ->createQueryBuilder('sg')
            ->select('sg.id')
            ->innerJoin('sg.teachings', 't')
            ->where('t.course = :courseParam');

        $availableGroupList = $groupRepository
            ->createQueryBuilder('g')
            ->where('g.path is not null and g.path = :pathParam')
            ->setParameter(':pathParam', $course->getPath())
            ->andWhere($notAvailableGroupListQueryBuilder->expr()->notIn('g.id', $notAvailableGroupListQueryBuilder->getDQL()))
            ->setParameter(':courseParam', $course)
            ->getQuery()
            ->getResult();

        $availableGroupListData = $this->serializer->serialize($availableGroupList, 'json', ['groups' => ['group_list']]);

        return new JsonResponse($availableGroupListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}/teachings",
     *     name="organization_admin_get_organization_training_course_teaching_list",
     *     methods={"GET"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     */
    public function getOrganizationTrainingCourseTeachingList(Request $request, Organization $organization, Training $training, Session $session, Course $course): JsonResponse
    {
        $courseListData = $this->serializer->serialize($course->getTeachings(), 'json', ['groups' => ['teaching_list', 'user_list']]);

        return new JsonResponse($courseListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/courses/{course_id}/teachings/{group_slug}",
     *     name="organization_admin_delete_organization_training_course_teaching",
     *     methods={"DELETE"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("course", options={"mapping": {"course_id": "id", "session": "session"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "training": "training"}})
     * @ParamConverter("teaching", options={"mapping": {"group": "group", "course": "course"}})
     */
    public function deleteOrganizationTrainingCourseTeaching(Request $request, Organization $organization, Training $training, Session $session, Course $course, Group $group, Teaching $teaching): JsonResponse
    {
        $this->entityManager->remove($teaching);
        $this->entityManager->flush();

        return new JsonResponse('', 200, [], true);
    }
}
