<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin\Organization\Training\Group;

use App\Entity\Group;
use App\Entity\Mark;
use App\Entity\Organization;
use App\Entity\Path;
use App\Entity\Session;
use App\Entity\Student;
use App\Entity\Test;
use App\Entity\Training;
use App\Repository\GroupRepository;
use App\Repository\TestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingGroupController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups",
     *     name="admin_get_organization_training_groups",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingGroups(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var GroupRepository $groupRepository */
        $groupRepository = $this->entityManager
            ->getRepository(Group::class);

        $groupsQueryBuilder = $groupRepository
            ->createQueryBuilder('g')
            ->where('g.session = :sessionParam')
            ->setParameter(':sessionParam', $session);

        $groupsQuery = $groupsQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $groupsQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $groupsPaginator = new Paginator($groupsQuery);

        $totalItems = $groupsPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $groupsPaginatorData = [
            'items' => $groupsPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $groupsData = $this->serializer->serialize($groupsPaginatorData, 'json', ['groups' => ['group_list', 'group_list_path']]);

        return new JsonResponse($groupsData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups/{group_slug}",
     *     name="admin_get_organization_training_group",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "session": "session"}})
     */
    public function getOrganizationTrainingGroup(Organization $organization, Training $training, Session $session, Group $group): JsonResponse
    {
        $groupData = $this->serializer->serialize($group, 'json', ['groups' => ['group_read', 'group_read_path']]);

        return new JsonResponse($groupData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups",
     *     name="admin_create_organization_training_group",
     *     methods={"POST"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function createOrganizationTrainingGroup(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        $content = $request->getContent();
        $contentObject = json_decode($content);

        /** @var Group $group */
        $group = $this->serializer->deserialize(
            $content,
            Group::class,
            'json',
            [
                'groups' => ['group_create'],
            ]);

        if (property_exists($contentObject, 'path')) {
            switch (gettype($contentObject->path)) {
                case 'NULL':
                    $group->setPath(null);
                    break;
                case 'integer':
                    $pathId = $contentObject->path;

                    /** @var Path|null $path */
                    $path = $this->entityManager
                        ->getRepository(Path::class)
                        ->findOneBy([
                            'id' => $pathId,
                            'session' => $session,
                        ]);

                    if (null === $path) {
                        throw new NotFoundHttpException('No path found for id '.$pathId);
                    }

                    $group->setPath($path);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong path id.');
            }
        }

        $group->setSession($session);

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        $groupData = $this->serializer->serialize($group, 'json', ['groups' => ['group_read', 'group_read_path']]);

        return new JsonResponse($groupData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups/{group_slug}",
     *     name="admin_delete_organization_training_group",
     *     methods={"DELETE"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "session": "session"}})
     */
    public function deleteOrganizationTrainingGroup(Request $request, Organization $organization, Training $training, Session $session, Group $group): JsonResponse
    {
        $this->entityManager->remove($group);
        $this->entityManager->flush();

        return new JsonResponse('', 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups/{group_slug}",
     *     name="admin_update_organization_training_group",
     *     methods={"PUT"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "session": "session"}})
     */
    public function updateOrganizationTrainingGroup(Request $request, Organization $organization, Training $training, Session $session, Group $group): JsonResponse
    {
        $content = $request->getContent();
        $contentObject = json_decode($content);

        /** @var Group $group */
        $group = $this->serializer->deserialize(
            $content,
            Group::class,
            'json',
            [
                'groups' => ['group_create'],
                'object_to_populate' => $group,
            ]);

        if (property_exists($contentObject, 'path')) {
            switch (gettype($contentObject->path)) {
                case 'NULL':
                    $group->setPath(null);
                    break;
                case 'integer':
                    $pathId = $contentObject->path;

                    $path = $this->entityManager
                        ->getRepository(Path::class)
                        ->findOneBy([
                            'id' => $pathId,
                            'session' => $session,
                        ]);

                    if (!$path) {
                        throw new NotFoundHttpException('No path found for id '.$pathId);
                    }

                    $group->setPath($path);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong path id.');
            }
        }

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        $groupData = $this->serializer->serialize($group, 'json', ['groups' => ['group_read', 'group_read_path']]);

        return new JsonResponse($groupData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups/{group_slug}/students/{student_email}",
     *     name="admin_attach_organization_training_group_student",
     *     methods={"POST"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "session": "session"}})
     * @ParamConverter("student", options={"mapping": {"student_email": "email", "organization": "organization"}})
     */
    public function attachOrganizationTrainingGroupStudent(Organization $organization, Training $training, Session $session, Group $group, Student $student): JsonResponse
    {
        // TODO: check if user have not group with same path

        $group->addStudent($student);

//        /** @var TestRepository $testRepository */
//        $testRepository = $this->entityManager
//            ->getRepository(Test::class);
//
//        $studentTestIdsQuery = $testRepository
//            ->createQueryBuilder('tb')
//            ->select('tb.id')
//            ->innerJoin('tb.marks', 'm')
//            ->where('m.student = :studentParam');
//
//        $testsQueryBuilder = $testRepository
//            ->createQueryBuilder('t')
//            ->innerJoin('t.teaching', 'tch')
//            ->where('tch.group = :groupParam');
//
//        $tests = $testsQueryBuilder
//            ->andWhere($testsQueryBuilder->expr()->notIn('t.id', $studentTestIdsQuery->getDQL()))
//            ->setParameter(':studentParam', $student)
//            ->setParameter(':groupParam', $group)
//            ->getQuery()
//            ->getResult();
//
//        foreach ($tests as $test) {
//            $mark = new Mark();
//            $mark->setStudent($student);
//            $mark->setTest($test);
//
//            $this->entityManager->persist($mark);
//        }

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        $studentData = $this->serializer->serialize($group, 'json', ['groups' => 'user_read']);

        return new JsonResponse($studentData, 201, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups/{group_slug}/students/{student_email}",
     *     name="admin_detach_organization_training_group_student",
     *     methods={"DELETE"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "session": "session"}})
     * @ParamConverter("student", options={"mapping": {"student_email": "email", "group": "group"}})
     */
    public function detachOrganizationTrainingGroupStudent(Organization $organization, Training $training, Session $session, Group $group, Student $student): JsonResponse
    {
        $group->removeStudent($student);

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        return new JsonResponse('', 204, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Group")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/groups/{group_slug}/students",
     *     name="admin_get_organization_training_group_students",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("group", options={"mapping": {"group_slug": "slug", "session": "session"}})
     */
    public function getOrganizationTrainingGroupStudentList(Organization $organization, Training $training, Session $session, Group $group): JsonResponse
    {
        $studentListData = $this->serializer->serialize($group->getStudents(), 'json', ['groups' => 'user_read']);

        return new JsonResponse($studentListData, 200, [], true);
    }
}
