<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Doctrine\DBAL\Types;

class EnumOrganizationSectorType extends EnumType
{
    protected string $name = 'organization_sector';
    protected array $values = ['private', 'public'];
}
