<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use App\Entity\Organization;
use App\Entity\User;
use Faker\Factory as FakerFactory;

class Factory extends \Codeception\Module
{
    public function haveUser(string $class, array $parameters = []): User
    {
        $faker = FakerFactory::create();
        $doctrine = $this->getModule('Doctrine2');

        $parameters = array_merge([
            'firstName' => $faker->firstName,
            'lastName' => $faker->lastName,
            'phone' => $faker->phoneNumber,
            'email' => $faker->safeEmail,
            'plainPassword' => $faker->password,
        ], $parameters);

        $userId = $doctrine->haveInRepository($class, $parameters);

        return $doctrine->grabEntityFromRepository($class, ['id' => $userId]);
    }

    public function haveOrganization(array $parameters = []): Organization
    {
        $faker = FakerFactory::create();
        $doctrine = $this->getModule('Doctrine2');

        $parameters = array_merge([
            'name' => $faker->company,
            'slug' => $faker->word,
            'sector' => $faker->randomElement(['public', 'private']),
        ], $parameters);

        $organizationId = $doctrine->haveInRepository(Organization::class, $parameters);

        return $doctrine->grabEntityFromRepository(Organization::class, ['id' => $organizationId]);
    }
}
