<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\functional;

use App\Entity\Admin;
use App\Entity\OrganizationAdmin;
use App\Entity\Professor;
use App\Entity\Student;
use App\Tests\FunctionalTester;
use Codeception\Test\Unit;

class UserPasswordSubscriberTest extends Unit
{
    /**
     * @var FunctionalTester
     */
    protected $tester;

    public function testICreateUser()
    {
        $user = new Admin();
        $user->setEmail('brad.gibson@example.com');
        $user->setFirstName('Brad');
        $user->setLastName('Gibson');
        $user->setPlainPassword('firewall');

        $this->tester->assertEmpty($user->getPassword());

        $doctrine = $this->getModule('Doctrine2');
        $doctrine->em->persist($user);
        $doctrine->em->flush();

        $this->tester->assertNotNull($user->getPassword());
    }

    public function testIUpdatePassword()
    {
        $organization = $this->tester->haveOrganization();

        $user = $this->tester->haveUser(OrganizationAdmin::class, [
            'plainPassword' => 'fang',
            'organization' => $organization,
        ]);

        $password = $user->getPassword();

        $user->setPlainPassword('lennon');

        $doctrine = $this->getModule('Doctrine2');
        $doctrine->em->persist($user);
        $doctrine->em->flush();

        $this->tester->assertNotEquals($password, $user->getPassword());
    }

    public function testPasswordIsNotUpdatedIfNotChanged()
    {
        $organization = $this->tester->haveOrganization();

        $user = $this->tester->haveUser(Student::class, [
            'organization' => $organization,
        ]);

        $password = $user->getPassword();

        $user->setEmail('eelis.pulkkinen@example.com');

        $doctrine = $this->getModule('Doctrine2');
        $doctrine->em->persist($user);
        $doctrine->em->flush();

        $this->tester->assertEquals($password, $user->getPassword());
    }

    public function testPasswordIsEncoded()
    {
        $organization = $this->tester->haveOrganization();

        $user = $this->tester->haveUser(Professor::class, [
            'organization' => $organization,
            'plainPassword' => 'montecarlo',
        ]);

        $this->tester->assertNull($user->getPlainPassword());
        $this->tester->assertNotEquals('montecarlo', $user->getPassword());
    }
}
