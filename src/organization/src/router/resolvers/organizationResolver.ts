import {NavigationGuardNext, RouteLocationNormalized} from "vue-router";
import store from '@/store';
import organizationService from "@/services/organizationService";
import {notFoundRoute} from "@/helpers";

const organizationResolver = async (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {

    if(store.getters.hasCurrentOrganization) {
        return next();
    }

    if(to.name === "not-found") {
        return next()
    }

    try {

        const organization = await organizationService.getCurrent();
        store.commit('setCurrentOrganization', organization);

        return next()
    } catch (error) {
        return next(notFoundRoute(to));
    }
}

export default organizationResolver
