import {useStore} from "vuex";
import { computed } from 'vue';

export function useCurrentOrganization(){
    const store = useStore();
    return computed(() => store.getters.currentOrganization);
}
