import { createStore } from "vuex";

export default createStore({
  state: {
    currentUser: null,
    currentOrganization: null,
    resetPasswordToken: null,
    studentSessionList: null,
  },
  mutations: {
    setCurrentUser: (state, user) => {
      state.currentUser = user;
    },
    setResetPasswordToken: (state, user) => {
      state.resetPasswordToken = user;
    },
    setCurrentOrganization: (state, organization) => {
      state.currentOrganization = organization;
    },
    setStudentSessionList: (state, sessionList) => {
      state.studentSessionList = sessionList;
    }
  },
  actions: {},
  getters: {
    currentUser: state => state.currentUser,
    currentOrganization: state => state.currentOrganization,
    hasCurrentUser: state => state.currentUser !== null,
    hasCurrentOrganization: state => state.currentOrganization !== null,
    resetPasswordToken: state => state.resetPasswordToken,
    studentSessionList: state => state.studentSessionList,
  },
  modules: {}
});
