import axios from 'axios';
import {Organization} from "@/types";

export default {
    getAll(): Promise<Organization[]> {
        return new Promise((resolve, reject) => {
            axios
                .get('/organizations')
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    get(organizationSlug: string): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/organizations/${organizationSlug}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getCurrent(): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/organizations`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
};
