import axios from 'axios';
import {Course, Group, Mark, Teaching} from "@/types";
import {Session} from "@/types";

export default {
    getSessions(): Promise<Session[]> {
        return new Promise((resolve, reject) => {
            axios
                .get('/student/sessions')
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getSession(schoolYear: string): Promise<Session> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/student/sessions/${schoolYear}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getSessionTeachings(schoolYear: string): Promise<Group[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/student/${schoolYear}/teachings`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getSessionCourseList(schoolYear: string): Promise<Course[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/student/${schoolYear}/courses`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    // getTeaching(teachingId: number): Promise<Teaching> {
    //     return new Promise((resolve, reject) => {
    //         axios
    //             .get(`/student/teachings/${teachingId}`)
    //             .then(response => {
    //                 resolve(response.data);
    //             })
    //             .catch(error => {
    //                 reject(error.response?.data);
    //             });
    //     });
    // },
    // getTeachingTests(teachingId: number): Promise<Mark[]> {
    //     return new Promise((resolve, reject) => {
    //         axios
    //             .get(`/student/teachings/${teachingId}/tests`)
    //             .then(response => {
    //                 resolve(response.data);
    //             })
    //             .catch(error => {
    //                 reject(error.response?.data);
    //             });
    //     });
    // },
    getMarks(schoolYear : string): Promise<Mark[][]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/student/${schoolYear}/marks`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
};
