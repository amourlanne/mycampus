import axios from 'axios';
import {User} from "@/types";

export default {
    login(username: string, password: string): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .post('/admin/login', { username, password })
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    authenticate() {
        return new Promise((resolve, reject) => {
            axios
                .get('/admin/authenticate')
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getAll(): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get('/users')
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getAdminAll(): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get('/admin/users')
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
};
