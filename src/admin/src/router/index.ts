import {createRouter, createWebHistory, Router, RouteRecordRaw} from "vue-router";
import NotFound from "@/views/NotFound.vue";
import OrganizationTrainingGroupList from "@/views/dashboard/organization/training/group/OrganizationTrainingGroupList.vue";
import OrganizationTrainingGroupStudentList
  from "@/views/dashboard/organization/training/group/student/OrganizationTrainingGroupStudentList.vue";
import OrganizationTrainingGroupStudentAttach
  from "@/views/dashboard/organization/training/group/student/OrganizationTrainingGroupStudentAttach.vue";
import OrganizationTrainingGroupCreate
  from "@/views/dashboard/organization/training/group/OrganizationTrainingGroupCreate.vue";
import OrganizationTrainingPathList
  from "@/views/dashboard/organization/training/path/OrganizationTrainingPathList.vue";
import OrganizationTrainingPathCreate
  from "@/views/dashboard/organization/training/path/OrganizationTrainingPathCreate.vue";
import OrganizationTrainingPathGroupList
  from "@/views/dashboard/organization/training/path-group/OrganizationTrainingPathGroupList.vue";
import OrganizationTrainingPathGroupCreate
  from "@/views/dashboard/organization/training/path-group/OrganizationTrainingPathGroupCreate.vue";
import OrganizationTrainingPathGroupEdit
  from "@/views/dashboard/organization/training/path-group/OrganizationTrainingPathGroupEdit.vue";
import OrganizationTrainingCourseList
  from "@/views/dashboard/organization/training/course/OrganizationTrainingCourseList.vue";
import OrganizationTrainingCourseCreate
  from "@/views/dashboard/organization/training/course/OrganizationTrainingCourseCreate.vue";
import authenticationResolver from "@/router/resolvers/authenticationResolver";
import authenticationGuard from "@/router/guards/authenticationGuard";
import OrganizationTrainingStudentList
  from "@/views/dashboard/organization/training/student/OrganizationTrainingStudentList.vue";
import OrganizationTrainingCourseTeachingCreate
  from "@/views/dashboard/organization/training/course/teaching/OrganizationTrainingCourseTeachingCreate.vue";
import OrganizationTrainingCourseTeachingList
  from "@/views/dashboard/organization/training/course/teaching/OrganizationTrainingCourseTeachingList.vue";
import OrganizationTrainingTeachingList
  from "@/views/dashboard/organization/training/teaching/OrganizationTrainingTeachingList.vue";
import OrganizationTrainingCourseTeachingEdit
  from "@/views/dashboard/organization/training/course/teaching/OrganizationTrainingCourseTeachingEdit.vue";
import OrganizationTrainingTestList
  from "@/views/dashboard/organization/training/test/OrganizationTrainingTestList.vue";
import OrganizationTrainingTeachingEdit
  from "@/views/dashboard/organization/training/teaching/OrganizationTrainingTeachingEdit.vue";
import OrganizationTrainingTeachingTestList
  from "@/views/dashboard/organization/training/teaching/test/OrganizationTrainingTeachingTestList.vue";
import OrganizationTrainingTeachingTestCreate
  from "@/views/dashboard/organization/training/teaching/test/OrganizationTrainingTeachingTestCreate.vue";
import OrganizationTrainingTeachingTestEdit
  from "@/views/dashboard/organization/training/teaching/test/OrganizationTrainingTeachingTestEdit.vue";
import OrganizationTrainingTestEdit
  from "@/views/dashboard/organization/training/test/OrganizationTrainingTestEdit.vue";
import OrganizationTrainingTestMarkEdit
  from "@/views/dashboard/organization/training/test/mark/OrganizationTrainingTestMarkEdit.vue";
import OrganizationTrainingSession from "@/views/dashboard/organization/training/OrganizationTrainingSession.vue";
import OrganizationTrainingSessionLayout from "@/views/dashboard/organization/training/OrganizationTrainingSessionLayout.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    meta: {
      requiresAuth: true
    },
    component: () => import("../views/dashboard/DashboardLayout.vue"),
    children: [
      {
        path: "",
        name: "home",
        redirect: { name: "admin-organizations" },
      },
      {
        path: "users",
        name: "admin-users",
        component: () => import("../views/dashboard/UserList.vue"),
      },
      {
        path: "organizations",
        name: "admin-organizations",
        component: () => import("../views/dashboard/OrganizationList.vue"),
      },
      {
        path: "organizations/create",
        name: "admin-organization-create",
        component: () => import("../views/dashboard/OrganizationCreate.vue"),
      },
      {
        path: ":organizationSlug",
        component: () => import("../views/dashboard/organization/OrganizationLayout.vue"),
        children: [
          {
            path: "",
            name: "admin-organization",
            component: () => import("../views/dashboard/organization/OrganizationEdit.vue"),
          },
          {
            path: "trainings",
            name: "admin-organization-trainings",
            component: () => import("../views/dashboard/organization/OrganizationTrainingList.vue"),
          },
          {
            path: "students",
            name: "admin-organization-students",
            component: () => import("../views/dashboard/organization/users/student/OrganizationStudentList.vue"),
          },
          {
            path: "professors",
            name: "admin-organization-professors",
            component: () => import("../views/dashboard/organization/users/professor/OrganizationProfessorList.vue"),
          },
          {
            path: "admins",
            component: {
              template: `<router-view></router-view>`
            },
            children: [
              {
                path: "",
                name: "admin-organization-admins",
                component: () => import("../views/dashboard/organization/users/admin/OrganizationAdminList.vue"),
              },
              {
                path: "create",
                name: "admin-organization-admin-create",
                component: () => import("../views/dashboard/organization/users/admin/OrganizationAdminCreate.vue"),
              },
              {
                path: ":email",
                name: "admin-organization-admin-edit",
                component: () => import("../views/dashboard/organization/users/admin/OrganizationAdminEdit.vue"),
              },
            ]
          },
          {
            path: ":trainingSlug",
            component: () => import("../views/dashboard/organization/training/OrganizationTrainingLayout.vue"),
            children: [
              {
                path: "",
                name: "admin-organization-training",
                component: () => import("../views/dashboard/organization/training/OrganizationTrainingEdit.vue"),
              },
              {
                path: ":schoolYear",
                component: OrganizationTrainingSessionLayout as any,
                children: [
                  {
                    path: "",
                    name: "admin-organization-training-session",
                    component: OrganizationTrainingSession,
                  },
                  {
                    path: "groups",
                    component: {
                      template: `<router-view></router-view>`
                    },
                    children: [
                      {
                        path: "",
                        name: "organization-training-group-list",
                        component: OrganizationTrainingGroupList,
                      },
                      {
                        path: "create",
                        name: "organization-training-group-create",
                        component: OrganizationTrainingGroupCreate,
                      },
                      {
                        path: ":groupSlug",
                        name: "organization-training-group",
                        redirect: { name: "organization-training-group-student-list" },
                        component: () => import("../views/dashboard/organization/training/group/OrganizationTrainingGroupEdit.vue"),
                        children: [
                          {
                            path: "",
                            name: "organization-training-group-student-list",
                            component: OrganizationTrainingGroupStudentList,
                          },
                          {
                            path: "attach",
                            name: "organization-training-group-student-attach",
                            component: OrganizationTrainingGroupStudentAttach,
                          },
                        ]
                      },
                    ]
                  },
                  {
                    path: "paths",
                    component: {
                      template: `<router-view></router-view>`
                    },
                    children: [
                      {
                        path: "",
                        name: "organization-training-path-list",
                        component: OrganizationTrainingPathList,
                      },
                      {
                        path: "create",
                        name: "organization-training-path-create",
                        component: OrganizationTrainingPathCreate,
                      },
                      {
                        path: ":pathId",
                        name: "organization-training-path",
                        component: () => import("../views/dashboard/organization/training/path/OrganizationTrainingPathEdit.vue"),
                      },
                    ]
                  },
                  {
                    path: "path-groups",
                    component: {
                      template: `<router-view></router-view>`
                    },
                    children: [
                      {
                        path: "",
                        name: "organization-training-path-group-list",
                        component: OrganizationTrainingPathGroupList,
                      },
                      {
                        path: "create",
                        name: "organization-training-path-group-create",
                        component: OrganizationTrainingPathGroupCreate,
                      },
                      {
                        path: ":pathGroupId",
                        name: "organization-training-path-group",
                        component: OrganizationTrainingPathGroupEdit as any,
                      },
                    ]
                  },
                  {
                    path: "students",
                    name: "organization-training-student-list",
                    component: OrganizationTrainingStudentList
                  },
                  {
                    path: "courses",
                    component: {
                      template: `<router-view></router-view>`
                    },
                    children: [
                      {
                        path: "",
                        name: "organization-training-course-list",
                        component: OrganizationTrainingCourseList,
                      },
                      {
                        path: "create",
                        name: "organization-training-course-create",
                        component: OrganizationTrainingCourseCreate,
                      },
                      {
                        path: ":courseId",
                        component: () => import("../views/dashboard/organization/training/course/OrganizationTrainingCourseEdit.vue"),
                        children: [
                          {
                            path: "",
                            name: "organization-training-course",
                            component: OrganizationTrainingCourseTeachingList,
                          },
                          {
                            path: "teachings/create",
                            name: "organization-training-course-teaching-create",
                            component: OrganizationTrainingCourseTeachingCreate,
                          },
                          {
                            path: "teachings/:groupSlug",
                            name: "organization-training-course-teaching-edit",
                            component: OrganizationTrainingCourseTeachingEdit as any,
                          },
                        ]
                      },
                    ]
                  },
                  {
                    path: "teachings",
                    component: {
                      template: `<router-view></router-view>`
                    },
                    children: [
                      {
                        path: "",
                        name: "organization-training-teaching-list",
                        component: OrganizationTrainingTeachingList
                      },
                      {
                        path: ":teachingId",
                        component: OrganizationTrainingTeachingEdit as any,
                        children: [
                          {
                            path: "",
                            name: "organization-training-teaching-edit",
                            component: OrganizationTrainingTeachingTestList
                          },
                          {
                            path: "tests/create",
                            name: "organization-training-teaching-test-create",
                            component: OrganizationTrainingTeachingTestCreate
                          },
                          {
                            path: "tests/:testId",
                            name: "organization-training-teaching-test-edit",
                            component: OrganizationTrainingTeachingTestEdit as any,
                          },
                        ]
                      }
                    ]
                  },
                  {
                    path: "tests",
                    component: {
                      template: `<router-view></router-view>`
                    },
                    children: [
                      {
                        path: "",
                        name: "organization-training-test-list",
                        component: OrganizationTrainingTestList
                      },
                      {
                        path: ":testId",
                        name: "organization-training-test-edit",
                        component: OrganizationTrainingTestEdit as any,
                        children: [
                          {
                            path: "marks/:markId",
                            name: "organization-training-test-mark-edit",
                            component: OrganizationTrainingTestMarkEdit as any
                          },
                        ]
                      }
                    ]
                  },
                ]
              },

            ]
          }
        ]
      },
    ]
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'not-found',
    component: NotFound
  }
];

const router: Router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach(authenticationResolver);
router.beforeEach(authenticationGuard);

export default router;
