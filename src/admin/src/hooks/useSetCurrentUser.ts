import {useStore} from "vuex";
import {User} from "@/types";

export function useSetCurrentUser(){
    const store = useStore();
    return (user: User|null) => store.commit('setCurrentUser', user);
}
