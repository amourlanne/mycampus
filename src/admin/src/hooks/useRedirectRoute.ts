import {useStore} from "vuex";
import { computed } from 'vue';
import {RouteLocationNormalized} from "vue-router";
import {User} from "@/types";

export function useRedirectRoute() {
    const store = useStore();
    return [
        computed(() => store.getters.redirectRoute).value as any,
        (redirectRoute: any) => store.commit('setRedirectRoute', redirectRoute)
    ];
}
