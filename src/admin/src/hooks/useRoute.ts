import {Route} from "@/types";
import { useRoute as baseUseRoute } from "vue-router";

export function useRoute() {
    return baseUseRoute() as Route;
}
