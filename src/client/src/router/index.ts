import {createRouter, createWebHistory, Router, RouteRecordRaw} from "vue-router";
import Home from "../views/Home.vue";
import NotFound from "@/views/NotFound.vue";
import authenticationResolver from "@/router/resolvers/authenticationResolver";
import authenticationGuard from "@/router/guards/authenticationGuard";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'not-found',
    component: NotFound
  }
];

const router: Router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach(authenticationResolver);
router.beforeEach(authenticationGuard);

export default router;
