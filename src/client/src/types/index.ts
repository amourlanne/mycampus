import { ComponentPublicInstance as BaseComponentPublicInstance } from 'vue';
import {RouteLocationNormalized as BaseRouteLocationNormalized, RouteLocationNormalizedLoaded} from "vue-router";

interface Data {
    [key: string]: unknown
}

export type Props = Data

export interface Params {
    [key: string]: string
}
export type RouteQueryParams = Params

export interface Environment {
    [key: string]: string
}

export interface QueryParams {
    [key: string]: string|number
}

export interface Route extends RouteLocationNormalizedLoaded {
    query: RouteQueryParams,
    params: Params
}

export interface RouteLocationNormalized extends BaseRouteLocationNormalized {
    query: RouteQueryParams,
    params: Params
}

export interface SetupContext {
    attrs: Data
    emit: (event: string, ...args: unknown[]) => void
}

export interface ComponentPublicInstance extends BaseComponentPublicInstance, Data {
}

export interface ModelCollection<T> {
    items: T[],
    totalItems: number,
    pages: number
}

// model
export interface Organization {
    slug: string;
    name: string;
    sector: string;
}

export interface Training {
    slug: string;
    name: string;
}

export interface Group {
    slug: string;
    name: string;
    studentCount: number;
}

export interface Path {
    id: number;
    name: string;
}

export interface Course {
    id: number;
    name: string;
    path?: {
        id?: number,
        name: string
    }
}

export interface User {
    fullName: string;
    firstName: string;
    lastName: string;
    email: string;
}


