import axios from 'axios';
import {Organization} from "@/types";

export default {
    getAll(): Promise<Organization[]> {
        return new Promise((resolve, reject) => {
            axios
                .get('/organizations')
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    get(organizationSlug: string): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/organizations/${organizationSlug}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganization(organizationSlug: string): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getCurrent(): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/organizations`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getAdminAll(): Promise<Organization[]> {
        return new Promise((resolve, reject) => {
            axios
                .get('/admin/organizations')
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    create(organizationData: Organization): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .post('/admin/organizations', organizationData)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    delete(organizationSlug: string): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    update(organizationSlug: string, organizationData: Organization): Promise<Organization> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}`, organizationData)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
};
