import axios from 'axios';
import {Path, Group, ModelCollection, QueryParams, RouteQueryParams, Training, User, Course} from "@/types";

export default {
    login(username: string, password: string): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .post('/admin/login', { username, password })
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationAdmin(organizationSlug: string, adminEmail: string): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/admins/${adminEmail}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationAdmin(organizationSlug: string, adminData: User): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/admins`, adminData)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationAdmin(organizationSlug: string, adminEmail: string, adminData: User): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/admins/${adminEmail}`, adminData)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationAdmin(organizationSlug: string, adminEmail: string): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/admins/${adminEmail}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationAdmins(organizationSlug: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/admins`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationProfessors(organizationSlug: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/professors`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationStudents(organizationSlug: string, params: QueryParams ): Promise<ModelCollection<User>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/students`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainings(organizationSlug: string, params: RouteQueryParams ): Promise<ModelCollection<Training>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTraining(organizationSlug: string, trainingSlug: string ): Promise<Training> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingGroups(organizationSlug: string, trainingSlug: string, params: QueryParams ): Promise<ModelCollection<Group>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, groupSlug: string ): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups/${groupSlug}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, groupData: Group): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups`, groupData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, groupSlug: string, groupData: Group): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups/${groupSlug}`, groupData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, groupSlug: string): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups/${groupSlug}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingGroupStudents(organizationSlug: string, trainingSlug: string, groupSlug: string ): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups/${groupSlug}/students`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    detachOrganizationTrainingGroupStudent(organizationSlug: string, trainingSlug: string, groupSlug: string, studentEmail: string ) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups/${groupSlug}/students/${studentEmail}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    attachOrganizationTrainingGroupStudent(organizationSlug: string, trainingSlug: string, groupSlug: string, studentEmail: string ) {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/groups/${groupSlug}/students/${studentEmail}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingPathList(organizationSlug: string, trainingSlug: string, params: QueryParams ): Promise<ModelCollection<Path>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/paths`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, pathId: number ): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/paths/${pathId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, pathData: Path): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/paths`, pathData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, pathId: number): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/paths/${pathId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, pathId: number, pathData: Path): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/paths/${pathId}`, pathData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingCourseList(organizationSlug: string, trainingSlug: string, params: QueryParams ): Promise<ModelCollection<Course>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/courses`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, courseId: number ): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/courses/${courseId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, courseData: Course): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/courses`, courseData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, courseId: number): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/courses/${courseId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, courseId: number, courseData: Course): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/courses/${courseId}`, courseData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
};
