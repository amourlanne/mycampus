import { createStore } from "vuex";

export default createStore({
  state: {
    currentUser: null,
  },
  mutations: {
    setCurrentUser: (state, user) => {
      state.currentUser = user;
    },
  },
  actions: {},
  getters: {
    currentUser: state => state.currentUser,
    hasCurrentUser: state => state.currentUser !== null,
  },
  modules: {}
});
